#!/usr/bin/env bash

#
# Definindo o Hostname do servidor
#
printf "\nDefina o Hostname para o servidor: "
read -r HOST_NAME

#
# Definindo o IP do servidor
#
printf "\nDefina o IP para o servidor: "
read -r IPADDR

#
# Definindo o IP do Gateway
#
printf "\nDefina o IP para o Gateway: "
read -r IPGW

printf "\nRealizando as configurações iniciais ...\n"

#
# Removendo arquivos iniciais
#
{
  > /etc/issue
  > /etc/motd
  /usr/bin/rm -rf /root/anaconda-ks.cfg
  /usr/bin/rm -rf /root/openscap_data
} &> /dev/null
printf "\nRemovendo arquivos iniciais.............ok"

#
# Atualizando o sistema
#
{
  /usr/bin/yum -y update
} &> /dev/null
printf "\nAtualizando o sistema...................ok"

#
# Pacotes iniciais
#
{
  /usr/bin/yum -y install epel-release
  /usr/bin/yum -y update
  /usr/bin/yum -y install htop
  /usr/bin/yum -y install vim \
  net-tools \
  bind-utils \
  bridge-utils \
  nc \
  tcpdump \
  traceroute \
  ccze \
  psmisc \
  tmux
} &> /dev/null
printf "\nInstalando programas iniciais...........ok"

#
# Instalando e configurndo o VMwareTools
#
{
  /usr/bin/tar xzf ./vmware-tools-distrib.tar.gz -C /opt/
  /bin/perl /opt/vmware-tools-distrib/vmware-install.pl --default --force-install
} &> /dev/null
printf "\nInstalando o VMwareTools................ok"

#
# Desabilitando o FirewallD
#
{
  /usr/bin/systemctl disable firewalld
} &> /dev/null
printf "\nDesabilitando o Firewalld...............ok"

#
# Desabilitando o NetworkManager
#
{
  /usr/bin/systemctl disable NetworkManager
} &> /dev/null
printf "\nDesabilitando o NetworkManager..........ok"

#
# Parando o FirewallD
#
{
  /usr/bin/systemctl stop firewalld
} &> /dev/null
printf "\nParando o Firewalld.....................ok"

#
# Parando o NetworkManager
#
{
  /usr/bin/systemctl stop NetworkManager
} &> /dev/null
printf "\nParando o NetworkManager................ok"

#
# Habilitando o Network
#
{
  /usr/bin/systemctl enable network
} &> /dev/mull
printf "\nHabilitando a interface de rede.........ok"

#
# Iniciando o Network
#
{
  /usr/bin/systemctl start network
} &> /dev/null
printf "\nConfigurando o serviço network..........ok"

#
# Configurando o arquivo ifcfg-eth0 com o IP Fixo definido
#
{
/usr/bin/cat <<EOF > /etc/sysconfig/network-scripts/ifcfg-eth0
DEVICE=eth0
NAME=eth0
ONBOOT=yes
IPADDR=${IPADDR}
PREFIX=24
GATEWAY=${IPGW}
DNS1=${IPGW}
DNS2=8.8.8.8
EOF
} &> /dev/null
printf "\nConfigurando a interface de rede........ok"

#
# Configurando o arquivo Hosts com o hostname e ip definido
#
{
/usr/bin/cat <<EOF > /etc/hosts

127.0.0.1   ${HOST_NAME} localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6

${IPADDR}   ${HOST_NAME}
EOF
} &> /dev/null
printf "\nConfigurando o arquivo hosts de rede....ok"

#
# Configurando o Hostname
#
{
/usr/bin/cat <<EOF > /etc/hostname
${HOST_NAME}
EOF
} &> /dev/null
printf "\nConfigurando o hostname.................ok"

#
# Configurando o Grub para que a interface de rede seja eth0
#
{
/usr/bin/cat <<EOF > /etc/default/grub
GRUB_TIMEOUT=0
GRUB_DISTRIBUTOR="$(sed 's, release .*$,,g' /etc/system-release)"
GRUB_DEFAULT=saved
GRUB_DISABLE_SUBMENU=true
GRUB_TERMINAL_OUTPUT="console"
GRUB_CMDLINE_LINUX="crashkernel=auto rd.lvm.lv=cl/root rd.lvm.lv=cl/swap rhgb quiet"
GRUB_DISABLE_RECOVERY="true"
GRUB_CMDLINE_LINUX="net.ifnames=0 biosdevname=0"
EOF

  /sbin/grub2-mkconfig -o /boot/grub2/grub.cfg
} &> /dev/null
printf "\nConfigurando o Grub.....................ok"

#
# Desabilitando IPv6 para o serviço SSH
#
{
/usr/bin/cat <<EOF > /etc/ssh/sshd_config
Port 22
AddressFamily inet
ListenAddress 0.0.0.0
HostKey /etc/ssh/ssh_host_rsa_key
HostKey /etc/ssh/ssh_host_ecdsa_key
HostKey /etc/ssh/ssh_host_ed25519_key
SyslogFacility AUTHPRIV
AuthorizedKeysFile      .ssh/authorized_keys
PasswordAuthentication yes
ChallengeResponseAuthentication no
UsePAM yes
AcceptEnv LANG LC_CTYPE LC_NUMERIC LC_TIME LC_COLLATE LC_MONETARY LC_MESSAGES
AcceptEnv LC_PAPER LC_NAME LC_ADDRESS LC_TELEPHONE LC_MEASUREMENT
AcceptEnv LC_IDENTIFICATION LC_ALL LANGUAGE
AcceptEnv XMODIFIERS
Subsystem       sftp    /usr/libexec/openssh/sftp-server
EOF

/usr/bin/ssh-keygen -t rsa -b 2048 -f /root/.ssh/id_rsa -q -P ""

/usr/bin/cat <<EOF >> /root/.ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCrw637hxatoUOqGDELEJdHMDEs9fhukihKIZsP9LB\
tFsGUs6SGXBHS7PLsCQD3bDwcjnK4eTNiroP6kAY23WQJpog2oTZ0BhAzpUiLNJe6WyndCijjb/dU4B\
ZXBGUyOApDcr44xQdJVX6g4osKawgbUP1gAHp7RTsdxk/PlE+nQvUjaMzLA8bE37r9sj/lslzBdtqYO\
A8TTCaZYROloR6QYPnrDcWz2YRmYi0282fuu1d1OP8YjRJDVNEhCRWju/ffopvnkzqswurfgI1XhMTy\
k5gZAp62i83vaxe+G16LTE2CcR8D/YKAwPlywYrejGXeN/EicnpfXbzRgm021A6P fabiano@latitude-e5570
EOF

/bin/chmod 0644 /root/.ssh/authorized_keys

} &> /dev/null
printf "\nConfigurando o SSH......................ok"

#
# Desabilitando IPv6 para o serviço Postfix
#
{
/usr/bin/cat <<EOF > /etc/postfix/main.cf
queue_directory = /var/spool/postfix
command_directory = /usr/sbin
daemon_directory = /usr/libexec/postfix
data_directory = /var/lib/postfix
mail_owner = postfix
inet_interfaces = localhost
inet_protocols = ipv4
mydestination = $myhostname, localhost.$mydomain, localhost
unknown_local_recipient_reject_code = 550
alias_maps = hash:/etc/aliases
alias_database = hash:/etc/aliases


debug_peer_level = 2
debugger_command =
         PATH=/bin:/usr/bin:/usr/local/bin:/usr/X11R6/bin
         ddd $daemon_directory/$process_name $process_id & sleep 5
sendmail_path = /usr/sbin/sendmail.postfix
newaliases_path = /usr/bin/newaliases.postfix
mailq_path = /usr/bin/mailq.postfix
setgid_group = postdrop
html_directory = no
manpage_directory = /usr/share/man
sample_directory = /usr/share/doc/postfix-2.10.1/samples
readme_directory = /usr/share/doc/postfix-2.10.1/README_FILES
EOF
} &> /dev/null
printf "\nConfigurando o Posfix...................ok"

#
# Limpando o histórico de comandos
#
{
  > ~root/.bash_history && history -c
} &> /dev/null
printf "\nLimpando o histórico de comandos........ok"

#
# Reinicializando o sistema para aplicar todas as alterações.
#
printf "\nReiniciando a máquina...................ok\n"
reboot

exit